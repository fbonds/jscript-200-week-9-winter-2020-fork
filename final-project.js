const formEl = document.getElementById('gif-me-form');


const BASE_URL = 'https://api.giphy.com/v1/gifs/search?';

class Person {
    constructor() {
        this.nameEl = document.getElementById('name');
        this.animalEl = document.getElementById('animal');
        this.moodEl = document.getElementById('mood')
    }
    popup() {
        window.alert("Searching for the animal that best defines YOU!");
    }
}

formEl.addEventListener('submit', function(e){
    e.preventDefault();
    thisPerson = new Person();

    const name = thisPerson.nameEl.value;
    const animal = thisPerson.animalEl.value;
    const mood = thisPerson.moodEl.value;
    const searchStr = mood + '%20' + animal;

    const url = `${BASE_URL}api_key=${API_KEY}&q=${searchStr}&limit=50&offset=0&rating=G&lang=en`;

    fetch(url)
        .then(function(data){
        return data.json();
    })
    .then(function(responseJson){
        let gifme = responseJson.data[Math.floor(Math.random() * responseJson.data.length)]
        let gifmeURL = gifme.embed_url
        const gifmeDiv = document.createElement('div');
        const gifmeP = document.createElement('p');
        const gifmeName = document.createElement('H1');
        const gifmeNameVal = document.createTextNode(name);
        gifmeName.appendChild(gifmeNameVal);
        const gifmeIframe = document.createElement('iframe');
        gifmeIframe.src = gifmeURL;
        gifmeIframe.width = '300';
        gifmeIframe.height = '300';
        gifmeIframe.frameBorder = '0';
        gifmeIframe.class = 'giphy-embed';
        gifmeP.appendChild(gifmeName);
        gifmeP.appendChild(gifmeIframe);
        gifmeDiv.appendChild(gifmeP);
        document.getElementById('gifs-container').prepend(gifmeDiv);
        thisPerson.popup();

    })
    

});

