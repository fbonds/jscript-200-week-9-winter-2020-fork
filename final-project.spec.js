describe('Gif Me', () => {
    describe('Tests', () => {
        
        it('Has an API KEY', () => {
            expect(API_KEY).toBeDefined();
        });

        it('Has Giphy API URL', () => {
            expect(BASE_URL).toContain('api.giphy.com');
        })

        it('Has Class', () => {
            const me = new Person();
            expect(me).toBeDefined();
        })

        it('Class has Method', () => {
            const me2 = new Person();
            expect(typeof me2.popup).toBe("function");
        })

    })
})